package com.graduate.model.mapper;

import com.graduate.model.domain.Member;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MemberMapper {

    public List<Member> findAll();
}
