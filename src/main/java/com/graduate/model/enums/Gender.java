package com.graduate.model.enums;

/**
 * 性别
 *
 * @author ____′↘夏悸
 */
public enum Gender {
    BOY, GIRL
}
